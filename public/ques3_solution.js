let textarea = document.querySelector('#input');
let button = document.querySelector('#resultBtn');
let result = document.querySelector('#result');

button.addEventListener('click', () => {
    let contents = textarea.value.trim();
    let converted = '';
    if (contents[0] === '#') {
        converted = hexToRGB(contents);
    } else {
        converted = rgbToHex(contents);
    }
    result.textContent = converted;
});

function hexToDecimal(hexValue) {
    let converted = 0;
    let hexMap = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        7: 7,
        8: 8,
        9: 9,
        a: 10,
        b: 11,
        c: 12,
        d: 13,
        e: 14,
        f: 15
    };
    hexValue = hexValue.toLowerCase();
    let hexLen = hexValue.length;
    for (let i = 0; i < hexLen; i++) {
        converted += hexMap[hexValue[hexLen - 1 - i]] * Math.pow(16, i);
    }
    return converted;
}

function hexToRGB(value) {
    let rgb = 'rgb(';
    for (let i = 1; i < 6; i += 2) {
        let sliced = value.slice(i, i + 2);
        rgb += hexToDecimal(sliced).toString() + ',';
    }
    return rgb.slice(0, rgb.length - 1) + ')';
}

function decimalToHex(decimalValue) {
    let decimalMap = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        7: 7,
        8: 8,
        9: 9,
        10: 'a',
        11: 'b',
        12: 'c',
        13: 'd',
        14: 'e',
        15: 'f'
    };
    let converted = '';
    let decimal = decimalValue;
    while (decimal !== 0) {
        let rem = Math.floor(decimal % 16);
        decimal = Math.floor(decimal / 16);
        converted += decimalMap[rem];
    }
    return converted
        .split('')
        .reverse()
        .join('');
}

function rgbToHex(rgbValue) {
    rgbValue = rgbValue.toLowerCase();
    let rgb = rgbValue
        .replace(/[^0-9]/gi, ' ')
        .trim()
        .split(' ');
    let converted = '';
    rgb.forEach(val => {
        converted += decimalToHex(val);
        if (converted.length < 2) {
            converted = '0' + converted;
        }
    });
    return '#' + converted;
}
