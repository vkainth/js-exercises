let textarea = document.querySelector('#input');
let button = document.querySelector('#resultBtn');
let result = document.querySelector('#result');

function findElementsWithSum(array, number) {
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length; j++) {
            if (i !== j) {
                if (array[i] + array[j] === number) {
                    return new Array(array[i], array[j]);
                }
            }
        }
    }
}

button.addEventListener('click', () => {
    let contents = textarea.value.trim().split('\n');
    let array = contents[0]
        .trim()
        .split(',')
        .map(Number);
    let number = Number(contents[1]);
    result.textContent = findElementsWithSum(array, number);
});
