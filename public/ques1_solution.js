let textarea = document.querySelector('#input');
let button = document.querySelector('#resultBtn');
let result = document.querySelector('#result');

function arrayToString(array) {
    let string = '';
    array.forEach(value => {
        if (typeof value === typeof []) {
            string += `[${value.toString()}],`;
        } else {
            string += `${value.toString()},`;
        }
    });
    return string;
}

button.addEventListener('click', () => {
    let contents = textarea.value
        .trim()
        .split(',')
        .map(Number);
    let cleanedArr = cleanRoom(contents);
    result.textContent = arrayToString(cleanedArr);
});

const similar = num1 => num2 => {
    return num1 === num2;
};

function cleanRoom(array) {
    // sort
    let sorted = array.sort((a, b) => a > b);
    // remove duplicates
    let unique = [...new Set(sorted)];
    let filteredArr = [];
    unique.forEach(value => {
        // find similar values
        let similarVal = similar(value);
        // filter the similar values
        let filtered = sorted.filter(similarVal);
        // add an array to filtered array if multiple items
        // else add the first item to flatten the array
        if (filtered.length > 1) {
            filteredArr.push(new Array(filtered));
        } else {
            filteredArr.push(filtered[0]);
        }
    });
    return filteredArr;
}

// let result = cleanRoom(arr);
// console.log(result);
