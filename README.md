# JavaScript Exercises

[https://vkainth.gitlab.io/js-exercises/](https://vkainth.gitlab.io/js-exercises/)

## Introduction

Three basic and straightforward exercises, with solutions written in JavaScript

## Technologies

- HTML and CSS

- JavaScript

- [Bulma](https://www.bulma.io)

## Attributions

- Stack Overflow
